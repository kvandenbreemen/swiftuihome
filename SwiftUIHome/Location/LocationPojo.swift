//
//  LocationPojo.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-11.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import Foundation

public struct LocationPojo {
    
    public let name: String
    public let key: String
    
    public init(name: String, key: String) {
        self.name = name
        self.key = key
    }
    
}
