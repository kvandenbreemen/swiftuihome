//
//  LocationView.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-11.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import SwiftUI

struct OverviewTopBar: View {
    
    var locationName: String
    
    var legacyViewControllerProvider: SwiftUILegacyViewProvider
    
    @State
    var settingsShowing: Bool = false
    
    var body: some View {
        HStack(spacing: 0.0) {
            Image("btn_settings_e").sheet(isPresented: $settingsShowing) {
                SimpleUIViewContainer(viewController: self.legacyViewControllerProvider.settingsViewController)
            }.onTapGesture {
                self.settingsShowing.toggle()
            }
            Spacer()
            Text(locationName)
                .fontWeight(.bold)
                .foregroundColor(Color.white)
                .font(.largeTitle)
                .multilineTextAlignment(.center)
                .lineLimit(1)
                
            Spacer()
            Image("btn_notifications")
            Image("icon_search")
            
        }.padding([.leading, .trailing], 10.0).padding([.top, .bottom], 5.0)
            .frame(maxHeight: 75)
    }
}

public struct LocationView: View {
    
    let viewModel: SwiftUIOverviewViewModel
    
    @State
    var showingHourlyForecast: Bool = false
    
    var backgroundImage: UIImage
    
    public init(viewModel: SwiftUIOverviewViewModel, backgroundImg: UIImage) {
        self.viewModel = viewModel
        self.backgroundImage = backgroundImg
    }
    
    public var body: some View {
        NavigationView {
            
            VStack {
                
                OverviewTopBar.init(locationName: viewModel.location.name, legacyViewControllerProvider: viewModel.legacyViewProvider)
                    
                
                ScrollView {
                    
                    VStack(spacing: 25) {
                        SwiftUIObservationsView(viewModel: viewModel.currentObservations)
                        
                        SwiftUITrendingNowView(viewModel: viewModel.trendingNow, provider: viewModel.interactorProvider)
                        
                        NavigationLink(destination: Text("Got here")) {
                            SwiftUIHourlyForecastOverviewCell(viewModel: viewModel.hourlyForecast)
                        }.buttonStyle(PlainButtonStyle())
                        
                        SwiftUIShortTermForecastOverviewCell(viewModel: viewModel.shortTermForecast)
                        
                    }.padding([.bottom], 100)
                }
            }.navigationBarTitle("").navigationBarHidden(true)
                .background(Image(uiImage: self.backgroundImage).resizable())
            
        }
    }
}

#if DEBUG

struct LocationView_Previews: PreviewProvider {
    
    static let location = LocationPojo.init(name: "Toronto", key: "CAON0696")
    
    static var previews: some View {
        let viewModel = SwiftUIOverviewViewModel(using: DefaultSwiftUIInteractorProvider(), with: DefaultSwiftUILegacyViewProvider())
        
        let ret = Group {
            
            OverviewTopBar(locationName: "Coquitlam", legacyViewControllerProvider: DefaultSwiftUILegacyViewProvider()).previewLayout(PreviewLayout.fixed(width: 375.0, height: 100.0))
            
            LocationView(viewModel: viewModel, backgroundImg: UIImage(named: "sui_bg_clearday")!).previewDevice("iPhone 11 Pro")
                .previewDisplayName("iPhone 11 Pro")
            
            LocationView(viewModel: viewModel, backgroundImg: UIImage(named: "sui_bg_clearday")!).previewDevice("iPhone 8 Plus")
                .previewDisplayName("iPhone 8 Pro")
            
        }.background(Image("sui_bg_clearday"))
        
        viewModel.update()
        
        return ret
    }
}
#endif
