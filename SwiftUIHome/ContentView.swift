//
//  ContentView.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-10.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack() {
            NetworkImageView.init(model: NetworkImageModel.init(with: MockImageInteractor()))
            Spacer()
            HStack {
                Spacer()
                CircleBarButton(label: "Home")
                CircleBarButton(label: "Map", imageAsset: "swiftui_icon_maps")
                CircleBarButton(label: "News", imageAsset: "swiftui_icon_news")
                CircleBarButton(label: "Video", imageAsset: "swiftui_icon_video")
                Spacer()
            }.background(Image("swiftui_bar_bg_murky").resizable(capInsets: EdgeInsets.init(), resizingMode: .stretch))
        }.edgesIgnoringSafeArea([
            .leading, .trailing, .bottom
        ])
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
        ContentView().previewDevice(PreviewDevice.init(rawValue: "iPhone X"))
            
            ContentView().previewDevice(PreviewDevice.init(rawValue: "iPhone XS Max"))
            
            ContentView().previewDevice(PreviewDevice.init(rawValue: "iPhone 8"))
            
            ContentView().previewDevice(PreviewDevice.init(rawValue: "iPhone 7"))
            
        }
    }
}
