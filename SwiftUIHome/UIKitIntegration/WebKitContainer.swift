//
//  WebKitContainer.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-04-19.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import UIKit
import WebKit
import SwiftUI
import Foundation

struct WebKitContainer: UIViewRepresentable {
    
    let request: URLRequest
    
    func updateUIView(_ webView: WKWebView, context: Context) {
        webView.load(request)
    }
    
    
    func makeUIView(context: Context) -> WKWebView {
        return WKWebView()
    }
    
    
    
}

struct WebKitContainer_Preview: PreviewProvider {
    
    
    static var previews: some View {
        
        let url = URL(string: "https://beta.theweathernetwork.com/ca/weather/ontario/toronto")!
        
        return WebKitContainer(request: URLRequest(url: url))
    }
    
}
