//
//  UIKitContainerDemo.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-04-14.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import SwiftUI

struct UIKitContainerCell: View {
 
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            SwiftUIOverviewCellHeaderLabel(label: "Embed UIViewController In SwiftUI")
            UIKitContainerDemo()
            SwiftUIOverviewMoreLabel()
        }
    }
}

struct UIKitContainerDemo: View {
    var body: some View {
        GeometryReader { geometry in
            UIViewContainer().frame(
                width: geometry.size.width,
                height: geometry.size.height
            )
        }
            
    }
}

struct UIKitContainerDemo_Previews: PreviewProvider {
    static var previews: some View {
        
        Group {
            UIKitContainerDemo().previewDisplayName("Container Proper")
            UIKitContainerCell().previewDisplayName("Container for Overview")
                .previewLayout(PreviewLayout.fixed(width: 320, height: 370))
                .background(Color.blue)
        }
        
        
    }
}

struct UIViewContainer: UIViewRepresentable {
    
    private var viewController = LegacyViewController()
    
    func makeUIView(context: Context) -> UIView {
        return viewController.view
    }
    
    func updateUIView(_ uiView: UIView, context: Context) {
        print("Received update instruction - context=\(context)")
    }
    
}
