//
//  LegacyViewController.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-04-14.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import UIKit

class LegacyViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func onPushTheForbiddenButton(_ sender: Any) {
        print("The forbidden button has been pressed 💩")
    }
}
