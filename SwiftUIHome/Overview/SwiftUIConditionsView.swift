//
//  SwiftUIConditionsView.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-18.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import SwiftUI

fileprivate struct HiLoView: View {
    
    var conds: SwiftUICurrentConditions
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack(spacing: 1) {
                Text("\(conds.yesterdayHigh)").modifier(TWNSwiftUIDesignSystem.CurrentConditionsValue())
                VStack(alignment: .leading) {
                    Text("°").modifier(TWNSwiftUIDesignSystem.CurrentConditionsUnitTitle())
                    Text("↑").modifier(TWNSwiftUIDesignSystem.CurrentConditionsUnitTitle())
                }
                Text("-").modifier(TWNSwiftUIDesignSystem.CurrentConditionsValue())
                Text("\(conds.yesterdayLow)").modifier(TWNSwiftUIDesignSystem.CurrentConditionsValue())
                VStack(alignment: .leading) {
                    Text("°").modifier(TWNSwiftUIDesignSystem.CurrentConditionsUnitTitle())
                    Text("↓").modifier(TWNSwiftUIDesignSystem.CurrentConditionsUnitTitle())
                }
            }
            
        }
    }
    
}

fileprivate struct ConditionView: View {
    
    var value: String
    var unitTop: String?
    var unitBottom: String
    var title: String
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            HStack(spacing: 1) {
                Text(value).modifier(TWNSwiftUIDesignSystem.CurrentConditionsValue())
                VStack(alignment: .leading) {
                    Text(unitTop ?? "").modifier(TWNSwiftUIDesignSystem.CurrentConditionsUnitTitle())
                    Text(unitBottom).modifier(TWNSwiftUIDesignSystem.CurrentConditionsUnitTitle())
                }
            }
            Text(title).modifier(TWNSwiftUIDesignSystem.CurrentConditionsTitle())
        }
    }
    
}

struct SwiftUIConditionsView: View {
    
    @ObservedObject
    var viewModel: SwiftUICurrentConditionsViewModel
    
    var body: some View {
        
        if let conds = viewModel.conditions {
            return AnyView(
                HStack {
                    Spacer()
                    VStack(alignment: .leading) {
                        Spacer()
                        ConditionView(value: "\(conds.wind)", unitTop: conds.windDirection, unitBottom: conds.units.wind, title: "WIND")
                        Spacer()
                        ConditionView(value: "\(conds.pressure)", unitTop: "↓", unitBottom: conds.units.pressure, title: "PRESSURE")
                        Spacer()
                        ConditionView(value: "\(conds.sunrise)", unitTop: "", unitBottom: "", title: "SUNRISE")
                        Spacer()
                    }
                    
                    VStack(alignment: .leading) {
                        Spacer()
                        ConditionView(value: "\(conds.windGust)", unitTop: "", unitBottom: conds.units.wind, title: "WIND GUST")
                        Spacer()
                        ConditionView(value: "\(conds.visibility)", unitTop: "", unitBottom: conds.units.visibility, title: "VISIBILITY")
                        Spacer()
                        ConditionView(value: "\(conds.sunset)", unitTop: "", unitBottom: "", title: "SUNSET")
                        Spacer()
                    }
                    
                    VStack(alignment: .leading) {
                        Spacer()
                        ConditionView(value: "\(conds.humidity)", unitTop: "", unitBottom: "%", title: "HUMIDITY")
                        Spacer()
                        ConditionView(value: "\(conds.ceiling)", unitTop: "", unitBottom: conds.units.ceiling, title: "CEILING")
                        Spacer()
                        VStack {
                            HiLoView(conds: conds)
                            Text("YESTERDAY").modifier(TWNSwiftUIDesignSystem.CurrentConditionsTitle())
                        }
                        Spacer()
                    }
                    Spacer()
                }
            )
        } else {
            return AnyView(SwiftUILoadingView())
        }
        
        
    }
}

struct SwiftUIConditionsView_Previews: PreviewProvider {
    static var previews: some View {
        
        let viewModel = SwiftUICurrentConditionsViewModel(with: SUCurrentConditionsInteractor(), for: LocationPojo(name: "Toronto", key: "CAON0696"))
        
        let ret = Group {
            
            //  Conditions
            SwiftUIConditionsView(viewModel: viewModel).previewDisplayName("Current Conditions")
            
            //  Hi Lo
            HiLoView(conds: SwiftUICurrentConditions.init(
            units: SwiftUIUnitsSettings.init(wind: "mph", pressure: "kPa", visibility: "km", ceiling: "m", rain: "in", snow: "in"),
                wind: 8, windDirection: "NE", windGust: 12, humidity: 80, pressure: 103.0, visibility: 24.0, ceiling: 10000, sunrise: "07:25", sunset: "19:30", yesterdayHigh: 8, yesterdayLow: 1)).previewDisplayName("High Low")
            
            //  Typical Condition
            ConditionView(value: "10", unitTop: "E", unitBottom: "kph", title: "WIND").previewDisplayName("Condition Detail Example")
        }
        .background(Color.blue)
        .previewLayout(PreviewLayout.fixed(width: 320.0, height: 229.0))
        
        viewModel.update()
        
        return ret
    }
}
