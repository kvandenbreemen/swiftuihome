//
//  SwiftUITrendingItem.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-05-03.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import Foundation

public enum TrendingItemType {
    case news
    case video
}

public struct SwiftUITrendingItem: Identifiable {
    
    public let id: UUID = UUID.init()
    let thumbnailURL: String
    let title: String
    let type: TrendingItemType
    
    public init(thumbnailURL: String, title: String, type: TrendingItemType) {
        self.thumbnailURL = thumbnailURL
        self.title = title
        self.type = type
    }
    
}
