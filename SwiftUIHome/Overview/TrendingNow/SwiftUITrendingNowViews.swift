//
//  SwiftUITrendingNowVideoCell.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-05-01.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import SwiftUI

struct SwiftUITrendingNowVideoCell: View {
    
    let item: SwiftUITrendingItem
    let provider: SwiftUIInteractorProvider
    
    var body: some View {
        
        let imageModel = NetworkImageModel.init(with: provider.imageInteractor)
        imageModel.update(with: item.thumbnailURL)
        
        return VStack(alignment: .center) {
            ZStack(alignment: .center) {
                NetworkImageView(model: imageModel)
                Image(systemName: "play.circle")
                    .scaleEffect(1.5)
                    .foregroundColor(Color.white)
                    .offset(x: -55, y: -45)
            }
            Text(item.title)
                .modifier(TWNSwiftUIDesignSystem.OverviewCellSmallText())
                .lineLimit(2)
        }.frame(width: 150, height: 170)
    }
}

struct SwiftUITrendingNowNewsCell: View {
    
    let item: SwiftUITrendingItem
    let provider: SwiftUIInteractorProvider
    
    var body: some View {
        
        let imageModel = NetworkImageModel.init(with: provider.imageInteractor)
        imageModel.update(with: item.thumbnailURL)
        
        return VStack(alignment: .center) {
            ZStack(alignment: .center) {
                NetworkImageView(model: imageModel)
            }
            Text(item.title)
                .modifier(TWNSwiftUIDesignSystem.OverviewCellSmallText())
                .lineLimit(2)
        }.frame(width: 150, height: 170)
    }
}

struct SwiftUITrendingNowView: View {
    
    @ObservedObject
    var viewModel: SwiftUITrendingNowViewModel
    
    var provider: SwiftUIInteractorProvider
    
    init(viewModel: SwiftUITrendingNowViewModel, provider: SwiftUIInteractorProvider) {
        self.viewModel = viewModel
        self.provider = provider
    }
    
    var body: some View {
        if let items = viewModel.stories {
            return AnyView(
            
                VStack {
                    SwiftUIOverviewCellHeaderLabel(label: "NEWS")
                    ScrollView(.horizontal, showsIndicators: false) {
                        HStack {
                            ForEach(items) { item in
                                if item.type == .news {
                                    SwiftUITrendingNowNewsCell(item: item, provider: self.provider)
                                } else {
                                    SwiftUITrendingNowVideoCell(item: item, provider: self.provider)
                                }
                            }
                        }
                    }
                }
            )
        } else {
            return AnyView(SwiftUILoadingView())
        }
    }
    
}

struct SwiftUITrendingNowVideoCell_Previews: PreviewProvider {
    static var previews: some View {
        
        let videoItem = SwiftUITrendingItem(thumbnailURL: "https://www.example.com", title: "Test Video Item with many lines", type: .video)
        let newsItem = SwiftUITrendingItem(thumbnailURL: "https://www.example.com", title: "Test News Item", type: .news)
        let interactorProvider = DefaultSwiftUIInteractorProvider()
        
        let viewModel = SwiftUITrendingNowViewModel.init(interactor: SUITrendingNowInteractor())
        
        let ret = Group {
            
            SwiftUITrendingNowVideoCell(item: videoItem, provider: interactorProvider).previewLayout(.fixed(width: 200, height: 200)).previewDisplayName("Video Item")
            SwiftUITrendingNowNewsCell(item: newsItem, provider: interactorProvider).previewLayout(.fixed(width: 200, height: 200)).previewDisplayName("News Item")
            SwiftUITrendingNowView(viewModel: viewModel, provider: interactorProvider).previewLayout(.fixed(width: 400, height: 200)).previewDisplayName("Trending Now List")
            
        }.background(TWNSwiftUIDesignSystem.Colors.transparent).background(Color.blue)
        
        viewModel.update()
        return ret
    }
}
