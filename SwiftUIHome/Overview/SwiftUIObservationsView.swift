//
//  ObservationsView.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-13.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import SwiftUI

struct SwiftUIObservationsView: View {
    
    @ObservedObject
    var viewModel: SwiftUICurrentObservationsViewModel
    
    var body: some View {
        
        if let obs = viewModel.obs {
            return AnyView(VStack(spacing: 0) {
                
                Spacer()
                
                HStack (alignment: VerticalAlignment.top){
                   
                    Spacer()
                    
                    Image(obs.wxIconStr).scaleEffect(0.6).offset(x: 30, y: -25).padding(EdgeInsets.init(top: 0, leading: 10, bottom: 0, trailing: 10))
                    Text("\(obs.temperature)").modifier(TWNSwiftUIDesignSystem.ObsCurrentTemperatureStyle())
                        
                    Text("°\(obs.tempUnits)").modifier(TWNSwiftUIDesignSystem.ObsTemperatureUnitStyle()).offset(x: -25.0, y: -5).alignmentGuide(.firstTextBaseline, computeValue: {d in d[.bottom]})
                        .padding(EdgeInsets.init(top: 0, leading: 10, bottom: 0, trailing: 0))
                    
                    Spacer()
                    
                }
                
                
                Text("Feels like \(obs.feelsLike)").modifier(TWNSwiftUIDesignSystem.ObsTemperatureFeelsLikeStyle())
                
                Text(obs.description).modifier(TWNSwiftUIDesignSystem.ObsCurrentConditions())
                
                Spacer()
                
            }.background(TWNSwiftUIDesignSystem.Colors.transparent))
        }
        else {
            return AnyView(SwiftUILoadingView())
        }
        
    }
}

#if DEBUG

let currentObs = SwiftUICurrentObservations(temperature: 23, tempUnits: "C", wxIconStr: "swiftui_sample_image_icon", feelsLike: 24, description: "Partly Cloudy and All Good")

struct ObservationsView_Previews: PreviewProvider {
    static var previews: some View {
        
        let interactor = SUCurrentObservationsInteractor.init()
        let viewModel = SwiftUICurrentObservationsViewModel.init(interactor: interactor, location: LocationPojo(name: "Toronto", key: "CAON0696"))
        
        let ret = Group {
            SwiftUIObservationsView(viewModel: viewModel).previewDevice("iPhone 11").previewDisplayName("iPhone 11")
            SwiftUIObservationsView(viewModel: viewModel).previewDevice("iPhone 8").previewDisplayName("iPhone 8")
            
            SwiftUIObservationsView(viewModel: viewModel).previewDevice("iPhone 8").previewDisplayName("iPhone 8+").previewLayout(.fixed(width: 414, height: 300))
            
        }
        .background(Image("sui_bg_clearday"))
        .previewLayout(PreviewLayout.fixed(width: 375.0, height: 229.0))
        
        viewModel.update()
        
        return ret
    }
}

#endif
