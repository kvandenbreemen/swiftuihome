//
//  SwiftUIHourlyForecastOverviewCell.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-25.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import SwiftUI

struct SwiftUIHourlyForecastOverviewPeriodCell: View {
    
    static let periodAlphas: [CGFloat] = [
        0.60,0.55,0.50,0.45,0.40,0.35,0.30,0.25,0.20,0.15,0.10,0.05,0.02,0.05,0.10,0.15,0.20,0.25,0.30,0.35,0.40,0.45,0.50,0.55
    ]
    
    var period: SwiftUIHourlyPeriod
    
    let chartHeight: CGFloat
    
    private func getBackgroundColor() -> Color {
        return Color(.sRGB, red: 0.0, green: 0.0, blue: 0.0, opacity: Double(SwiftUIHourlyForecastOverviewPeriodCell.periodAlphas[period.periodNumber]))
    }
    
    var body: some View {
        VStack(spacing: 0) {
            Text(period.time).modifier(TWNSwiftUIDesignSystem.OverviewCellSmallText())
            Image(period.icon)
            Text("\(period.temperature)°").modifier(TWNSwiftUIDesignSystem.OverviewCellLargeText())
            Text(period.feelsLike).modifier(TWNSwiftUIDesignSystem.OverviewCellSmallText())
            HStack {
                Image("icon_pop_cloud")
                Text("\(period.pop)%").modifier(TWNSwiftUIDesignSystem.OverviewCellSmallText())
            }
            AnimatedPrecipBarSegment(rain: self.period.rainHeight, snow: self.period.snowHeight, rainLabel: period.rainfallLabel, snowLabel: period.snowfallLabel)
                .frame(minHeight: chartHeight, maxHeight: chartHeight+1)
        }
        .padding([.top], 10)
        .background(getBackgroundColor())
        
    }
}

struct SwiftUIHourlyForecastOverviewCell: View {
    
    @ObservedObject
    var viewModel: SwiftUIOverviewHourlyViewModel
    
    private func computeMaxBarChartHeight(periods: [SwiftUIHourlyPeriod]) -> Int {
        var max = 0
        periods.forEach{ period in
            if period.snowHeight+period.rainHeight > max {
                max = period.snowHeight + period.rainHeight
            }
        }
        
        return max
    }
    
    private func buildPeriodGrid() -> some View {
        
        guard let periods = viewModel.periods else {
            return AnyView(SwiftUILoadingView())
        }
        
        let chartHeight = computeMaxBarChartHeight(periods: periods)
        
        return AnyView(ScrollView(.horizontal, showsIndicators: false) {
            HStack(spacing: 0) {
                
                ForEach(periods) { period in
                    SwiftUIHourlyForecastOverviewPeriodCell(period: period, chartHeight: CGFloat(chartHeight+70))
                }
                
            }
        }
        )   //  AnyView
        
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            SwiftUIOverviewCellHeaderLabel(label: "HOURLY")
            buildPeriodGrid()
            SwiftUIOverviewMoreLabel()
            
        }.background(TWNSwiftUIDesignSystem.Colors.transparent)
    }
}

struct SwiftUIHourlyForecastOverviewCell_Previews: PreviewProvider {
    
    static let provider: SwiftUIInteractorProvider = DefaultSwiftUIInteractorProvider()
    
    static var previews: some View {
        
        let unitSettings = SwiftUIUnitsSettings.init(wind: "mph", pressure: "kPa", visibility: "km", ceiling: "m", rain: "in", snow: "in")
        let viewModel = SwiftUIOverviewHourlyViewModel(interactor: provider.hourly, location: provider.location)
        
        let fakePeriod = SwiftUIHourlyPeriod(periodNumber: 0, time: "9am", icon: "swiftui_sample_small_icon", temperature: "12", feelsLike: "feels 11", pop: "20", rainHeight: 10, snowHeight: 1, rainfallLabel: "<1 mm", snowfallLabel: "~ 1cm", unitSettings: unitSettings, description: "Partly Cloudy", wind: 10, windGust: 24, windDirection: "se", rain: 10, snow: 3)
        
        let ret = Group {
            SwiftUIHourlyForecastOverviewPeriodCell(period: fakePeriod, chartHeight: 100)
            .previewLayout(PreviewLayout
                .fixed(width: 100, height: 346))
                .previewDisplayName("Period Cell")
            SwiftUIHourlyForecastOverviewCell(
                viewModel: viewModel
            )
                .previewLayout(PreviewLayout.fixed(width: 320, height: 370))
                .previewDisplayName("Overview Cell")
            
            SwiftUIHourlyForecastOverviewCell(
                viewModel: viewModel
            )
                .previewLayout(PreviewLayout.fixed(width: 320, height: 370))
                .previewDevice(PreviewDevice.init(rawValue: "iPhone 11 Pro"))
                .previewDisplayName("Overview Cell (iPhone 11 pro)")
            
        }.background(Image("sui_bg_clearday"))
        
        viewModel.update()
        
        return ret
        
    }
}
