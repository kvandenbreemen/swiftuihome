//
//  SwiftUILoadingView.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-17.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import SwiftUI

struct SwiftUILoadingView: View {
    
    @State var opacity: Double = 0.1
    
    var body: some View {
        HStack {
            Spacer()
            VStack {
                Spacer()
                Text("Loading....").foregroundColor(.white)
                Spacer()
            }
            Spacer()
        }.background(Color.black.opacity(opacity))
           
    }
}

struct SwiftUILoadingView_Previews: PreviewProvider {
    static var previews: some View {
        SwiftUILoadingView().background(Color.blue)
        .previewLayout(PreviewLayout.fixed(width: 320.0, height: 229.0))
    }
}
