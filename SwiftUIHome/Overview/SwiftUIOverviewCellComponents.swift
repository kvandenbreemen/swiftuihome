//
//  SwiftUIOverviewMoreLabel.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-27.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import SwiftUI

struct SwiftUIOverviewMoreLabel: View {
    var body: some View {
        HStack {
            Spacer()
            Text("MORE").modifier(TWNSwiftUIDesignSystem.ObsTemperatureFeelsLikeStyle())
            Image(systemName: "chevron.right.circle")
                .padding(.trailing, 10)
                .modifier(TWNSwiftUIDesignSystem.ObsTemperatureFeelsLikeStyle())
        }
        .padding(2)
        .background(TWNSwiftUIDesignSystem.Colors.clear)
    }
}

struct SwiftUIOverviewCellHeaderLabel: View {
    
    var label: String
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
            Text(label)
                .modifier(TWNSwiftUIDesignSystem.OverviewCellTitle())
                Spacer()
            }
        }.padding(2)
    }
}

struct SwiftUIOverviewMoreLabel_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            SwiftUIOverviewCellHeaderLabel(label: "EXAMPLE SECTION").previewDisplayName("Header Label")
            SwiftUIOverviewMoreLabel().previewDisplayName("'More' label")
        }
        .previewLayout(PreviewLayout.fixed(width: 200, height: 200))
        .background(Color.blue)
    }
}
