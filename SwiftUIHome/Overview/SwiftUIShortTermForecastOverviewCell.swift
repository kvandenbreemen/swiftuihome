//
//  SwiftUIShortTermForecastOverviewCell.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-04-06.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import SwiftUI

fileprivate struct SwiftUIShortTermForecastSegment: View {
    
    static let periodAlphas: [CGFloat] = [
        0.60,0.55,0.50,0.45
    ]
    
    let period: SwiftUIShortTermPeriod
    
    let chartHeight: CGFloat
    
    private func getBackgroundColor() -> Color {
        return Color(.sRGB, red: 0.0, green: 0.0, blue: 0.0, opacity: Double(SwiftUIHourlyForecastOverviewPeriodCell.periodAlphas[period.periodNumber]))
    }
    
    var body: some View {
        VStack(spacing: 5) {
            Text(period.label).modifier(TWNSwiftUIDesignSystem.OverviewCellTitle())
            Image(period.icon)
            Text("\(period.temperature)°").modifier(TWNSwiftUIDesignSystem.OverviewCellLargeText())
            Text(period.feelsLike).modifier(TWNSwiftUIDesignSystem.OverviewCellSmallText())
            HStack {
                Image("icon_pop_cloud")
                Text("\(period.pop)").modifier(TWNSwiftUIDesignSystem.OverviewCellSmallText())
            }
            AnimatedPrecipBarSegment(rain: self.period.rainHeight, snow: self.period.snowHeight, rainLabel: period.rainLabel, snowLabel: period.snowLabel)
            .frame(minHeight: chartHeight, maxHeight: chartHeight+1)
        }
        .frame(minWidth: 150)
        .padding([.top], 10)
        .background(getBackgroundColor())
    }
    
    
}

fileprivate struct ShortTermForecastGrid: View {
    
    var periods: [SwiftUIShortTermPeriod]
    
    private func computeMaxBarChartHeight(periods: [SwiftUIShortTermPeriod]) -> Int {
        var max = 0
        periods.forEach{ period in
            if period.snowHeight+period.rainHeight > max {
                max = period.snowHeight + period.rainHeight
            }
        }
        
        return max
    }
    
    var body: some View {
        
        let barGraphHeight = CGFloat(computeMaxBarChartHeight(periods: periods))
        
        return VStack(alignment: .leading, spacing: 0) {
            SwiftUIOverviewCellHeaderLabel(label: "Short-Term")
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 0) {
                    ForEach(periods) { period in
                        SwiftUIShortTermForecastSegment(period: period, chartHeight: barGraphHeight + 70)
                    }
                }
            }
            SwiftUIOverviewMoreLabel()
        }
        .background(TWNSwiftUIDesignSystem.Colors.transparent)
    }
    
}

struct SwiftUIShortTermForecastOverviewCell: View {
    
    @ObservedObject
    var viewModel: SwiftUIOverview36HourViewModel
    
    var body: some View {
        
        if viewModel.periods == nil {
            return AnyView(SwiftUILoadingView())
        } else {
            return AnyView(ShortTermForecastGrid(periods: viewModel.periods!))
        }
        
    }
}

struct SwiftUIShortTermForecastOverviewCell_Previews: PreviewProvider {
    static var previews: some View {
        
        let viewModel = SwiftUIOverview36HourViewModel(interactor: DefaultSwiftUIInteractorProvider().shortTerm)
        
        let period = SwiftUIShortTermPeriod(periodNumber: 0, label: "Evening", temperature: "42", icon: "swiftui_sample_image_icon", feelsLike: "Feels 42", pop: "50%", rainHeight: 24, snowHeight: 10, rainLabel: "~ 10mm", snowLabel: "1 cm")
        
        let ret = Group {
            
            SwiftUIShortTermForecastSegment(period: period, chartHeight: 100).previewDisplayName("Single Period").previewLayout(PreviewLayout.fixed(width: 150, height: 300))
            
            SwiftUIShortTermForecastOverviewCell(viewModel: viewModel)
                .previewDisplayName("Main Cell").previewLayout(PreviewLayout.fixed(width: 417, height: 400)).background(Image("sui_bg_clearday").scaleEffect(2.0))
            
            
            
        }.background(Image("sui_bg_clearday").scaledToFill())
        
        viewModel.update()
        
        return ret
        
    }
}
