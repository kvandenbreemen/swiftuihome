//
//  SwiftUIErrors.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-16.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import Foundation

public enum SwiftUIErrors: Error {
    
    case noConnection
    case locationNotFound
    case error(message: String)
    
}
