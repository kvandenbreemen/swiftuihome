//
//  Bumblebee.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-04-08.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import SwiftUI
import Foundation

public struct NewsStoryCallToAction: View {
    
    let headline: String
    
    let url: String
    
    @State
    private var animationAmount: CGFloat = 0.5
    
    @State
    private var isArticleOpen: Bool = false
    
    public init(headline: String, newsURL: String) {
        self.headline = headline
        self.url = newsURL
    }
    
    public var body: some View {
        
        HStack {
            ZStack {
                
                Circle()
                    .foregroundColor(Color.red)
                    .opacity(Double(2 - animationAmount))
                    .scaleEffect(1.5 * CGFloat(animationAmount)).animation(Animation.easeInOut(duration: 1.0).repeatForever().delay(1.0))
                    
                
                Circle().foregroundColor(Color.white)
                
                Circle()
                    .shadow(radius: 1.0)
                .foregroundColor(/*@START_MENU_TOKEN@*/.red/*@END_MENU_TOKEN@*/)
                    .scaleEffect(1.7)
                
                Text("!").font(.title).fontWeight(.bold).foregroundColor(/*@START_MENU_TOKEN@*/.white/*@END_MENU_TOKEN@*/)
                
            }.frame(maxWidth: 25, maxHeight: 25)
            .offset(x: 18.0, y: 0)
                .zIndex(2)
            
            VStack {
                Text(headline)
                    .font(.footnote)
                    .foregroundColor(/*@START_MENU_TOKEN@*/.white/*@END_MENU_TOKEN@*/)
            }
            .padding(.leading, 20).padding([.top, .bottom], 5)
            .frame(maxWidth: 150, maxHeight: 75)
            .background(TWNSwiftUIDesignSystem.Colors.darkBlue)
            .shadow(radius: 2.0)
            
            
        }
        .sheet(isPresented: $isArticleOpen) {
            WebKitContainer(request: URLRequest(url: URL(string: self.url)!))
        }
        .onTapGesture {
            self.isArticleOpen.toggle()
        }
        .onAppear {
            self.animationAmount = 2
        }
        
        
        
        
    }
    
}


struct NewsStoryCallToAction_Preview: PreviewProvider {
    static var previews: some View {
        
        NewsStoryCallToAction(headline: "Aliens land in Paris, Ontario, sparking Outrage", newsURL: "https://www.theweathernetwork.com/ca/news/article/ontario-mostly-fair-skies-on-the-weekend-slight-warmup-followed-by-cooldown")
            .background(Image("sui_bg_clearday"))
        
    }
    
}
