//
//  SwiftUIInteractorProvider.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-19.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import Foundation

/// Provider for giving the SwiftUI demo access to the data it needs to display stuff!
public protocol SwiftUIInteractorProvider {
    
    var currentObs: SUCurrentObservationsInteractorStrategy { get }
    var currentConds: SUCurrentConditionsInteractorStrategy { get }
    
    /// Get the current location you wish to use the SwiftUI system in
    var location: LocationPojo { get }
    
    var hourly: SUHourlyForecastInteractorStrategy { get }
    
    var shortTerm: SUIShortTermForecastInteractorStrategy { get }
    
    var imageInteractor: SwiftUIImageInteractorProtocol { get }
    
    var trendingNow: SUITrendingNowInteractorStrategy { get }
    
}

class DefaultSwiftUIInteractorProvider: SwiftUIInteractorProvider {
    var currentObs: SUCurrentObservationsInteractorStrategy {
        get {
            return SUCurrentObservationsInteractor()
        }
    }
    
    var currentConds: SUCurrentConditionsInteractorStrategy {
        get {
            return SUCurrentConditionsInteractor()
        }
    }
    
    var location: LocationPojo {
        get {
            return LocationPojo(name: "Toronto", key: "CAON0696")
        }
    }
    
    var hourly: SUHourlyForecastInteractorStrategy {
        get {
            return SUHourlyForecastInteractor()
        }
    }
    
    var shortTerm: SUIShortTermForecastInteractorStrategy {
        get {
            return SUIShortTermForecastInteractor()
        }
    }
    
    var imageInteractor: SwiftUIImageInteractorProtocol {
        get {
            return MockImageInteractor()
        }
    }
    
    var trendingNow: SUITrendingNowInteractorStrategy {
        get {
            return SUITrendingNowInteractor()
        }
    }
    
}
