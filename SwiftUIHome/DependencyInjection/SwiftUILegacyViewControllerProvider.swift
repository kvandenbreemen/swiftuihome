//
//  SwiftUILegacyViewControllerProvider.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-04-23.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import SwiftUI
import Foundation

public protocol SwiftUILegacyViewProvider {
    
    var settingsViewController: UIViewController { get }
    
}

class DefaultSwiftUILegacyViewProvider: SwiftUILegacyViewProvider {
    
    var settingsViewController: UIViewController {
        get {
            let vc = LegacyViewController.init()
            return vc
        }
    }
    
}

//  MARK:- Default container for any view controller
struct SimpleUIViewContainer: UIViewRepresentable {
    
    var viewController: UIViewController
    
    func makeUIView(context: Context) -> UIView {
        return viewController.view
    }
    
    func updateUIView(_ uiView: UIView, context: Context) {
        print("Received update instruction - context=\(context)")
    }
    
}
