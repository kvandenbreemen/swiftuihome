//
//  SwiftUIOverviewHourlyViewModel.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-27.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import Foundation

class SwiftUIOverviewHourlyViewModel: ObservableObject {
    
    struct Constants {
        static let defaultPeriods = 24
    }
    
    @Published var periods: [SwiftUIHourlyPeriod]?
    private let location: LocationPojo
    private let interactor: SUHourlyForecastInteractorStrategy
    private let numPeriods: Int
    
    init(interactor: SUHourlyForecastInteractorStrategy, location: LocationPojo, numPeriods: Int = Constants.defaultPeriods) {
        self.interactor = interactor
        self.location = location
        self.numPeriods = numPeriods
    }
    
    func update() {
        interactor.fetchHourlyForecast(for: location, with: numPeriods) { [weak self] (periods, errors) in
            guard let strongSelf = self else {
                return
            }
            
            if let periods = periods {
                strongSelf.periods = periods
            }
        }
    }
    
    
}
