//
//  SwiftUICurrentObservationsViewModel.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-17.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import Combine
import Foundation

class SwiftUICurrentObservationsViewModel: ObservableObject {
    
    private let interactor: SUCurrentObservationsInteractorStrategy
    private let location: LocationPojo
    
    @Published var obs: SwiftUICurrentObservations?
    
    init(interactor: SUCurrentObservationsInteractorStrategy, location: LocationPojo) {
        self.interactor = interactor
        self.location = location
        obs = nil
    }
    
    func update() {
        interactor.getCurrentObservations(for: location) { [weak self] (obs) in
            guard let self = self else {
                return
            }
            
            self.obs = obs
        }
    }
    
}
