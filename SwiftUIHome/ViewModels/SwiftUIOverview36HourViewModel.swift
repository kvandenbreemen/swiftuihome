//
//  SwiftUIOverview36HourViewModel.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-04-06.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import Foundation

class SwiftUIOverview36HourViewModel: ObservableObject {
    
    @Published
    var periods: [SwiftUIShortTermPeriod]? = nil
    
    private let interactor: SUIShortTermForecastInteractorStrategy
    
    private let numPeriods: Int
    
    init(interactor: SUIShortTermForecastInteractorStrategy, numPeriods: Int = 6) {
        self.interactor = interactor
        self.numPeriods = numPeriods
    }
    
    func update() {
        interactor.fetch(periods: numPeriods) { [weak self] (periods, error) in
            
            guard let strongSelf = self, let periods = periods else {
                return
            }
            
            strongSelf.periods = periods
            
        }
    }
    
}
