//
//  SwiftUITrendingNowViewModel.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-05-03.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import Foundation

class SwiftUITrendingNowViewModel: ObservableObject {
    
    @Published
    var stories: [SwiftUITrendingItem]?
    
    private var interactor: SUITrendingNowInteractorStrategy
    
    init(interactor: SUITrendingNowInteractorStrategy) {
        self.interactor = interactor
    }
    
    func update() {
        interactor.getStoriesAndVideos { [weak self] (items, errors) in
            guard let strongSelf = self else {
                return
            }
            
            if let items = items {
                strongSelf.stories = items
            }
        }
    }
    
}
