//
//  SwiftUICurrentConditionsViewModel.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-18.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import Foundation
import Combine

class SwiftUICurrentConditionsViewModel: ObservableObject {
    
    @Published var conditions: SwiftUICurrentConditions? = nil
    
    private let location: LocationPojo
    private let interactor: SUCurrentConditionsInteractorStrategy
    
    init(with interactor: SUCurrentConditionsInteractorStrategy, for location: LocationPojo) {
        self.interactor = interactor
        self.location = location
    }
    
    func update() {
        
        interactor.getCurrentConditions(location: location) { [weak self] (conditions) in
            
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.conditions = conditions
        }
        
        
    }
    
}
