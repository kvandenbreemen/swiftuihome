//
//  SwiftUIOverviewViewModel.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-19.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import Foundation

public class SwiftUIOverviewViewModel {
    
    let location: LocationPojo
    let currentObservations: SwiftUICurrentObservationsViewModel
    let currentConditions: SwiftUICurrentConditionsViewModel
    let hourlyForecast: SwiftUIOverviewHourlyViewModel
    let shortTermForecast: SwiftUIOverview36HourViewModel
    let trendingNow: SwiftUITrendingNowViewModel
    
    //  MARK:- Framework Integrations
    let interactorProvider: SwiftUIInteractorProvider
    let legacyViewProvider: SwiftUILegacyViewProvider
    
    //  MARK:- Class Proper
    
    public init(using provider: SwiftUIInteractorProvider, with viewProvider: SwiftUILegacyViewProvider) {
        location = provider.location
        
        legacyViewProvider = viewProvider
        interactorProvider = provider
        
        currentObservations = SwiftUICurrentObservationsViewModel(interactor: provider.currentObs, location: location)
        currentConditions = SwiftUICurrentConditionsViewModel(with: provider.currentConds, for: location)
        hourlyForecast = SwiftUIOverviewHourlyViewModel.init(interactor: provider.hourly, location: location)
        shortTermForecast = SwiftUIOverview36HourViewModel(interactor: provider.shortTerm)
        trendingNow = SwiftUITrendingNowViewModel.init(interactor: provider.trendingNow)
    }
    
    public func update() {
        currentObservations.update()
        currentConditions.update()
        hourlyForecast.update()
        shortTermForecast.update()
        trendingNow.update()
    }
    
}
