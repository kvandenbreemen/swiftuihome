//
//  SwiftUIHourlyPeriod.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-25.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import Foundation

public class SwiftUIHourlyPeriod: Identifiable {
    
    public let id: UUID
    let periodNumber: Int
    let time: String
    let icon: String
    let temperature: String
    let feelsLike: String
    let pop: String
    
    let rainHeight: Int
    let snowHeight: Int
    
    let rainfallLabel: String
    let snowfallLabel: String
    
    //  MARK:- Hourly Forecast Details
    let unitSettings: SwiftUIUnitsSettings
    let description: String
    let wind: Int
    let windDirection: String
    let windGust: Int
    let rain: Int
    let snow: Int
    
    //  MARK:- Constructor and stuff
    public init(periodNumber: Int, time: String, icon: String, temperature: String, feelsLike: String, pop: String, rainHeight: Int, snowHeight: Int, rainfallLabel: String, snowfallLabel: String,
                unitSettings: SwiftUIUnitsSettings,
                description: String, wind: Int, windGust: Int, windDirection: String, rain: Int, snow: Int
                ) {
        id = UUID()
        self.periodNumber = periodNumber
        self.time = time
        self.icon = icon
        self.temperature = temperature
        self.feelsLike = feelsLike
        self.pop = pop
        self.rainHeight = rainHeight
        self.snowHeight = snowHeight
        self.rainfallLabel = rainfallLabel
        self.snowfallLabel = snowfallLabel
        self.unitSettings = unitSettings
        self.description = description
        self.wind = wind
        self.windDirection = windDirection
        self.rain = rain
        self.snow = snow
        self.windGust = windGust
    }
    
}
