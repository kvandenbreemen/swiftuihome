//
//  SwiftUICurrentConditions.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-18.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import Foundation

public struct SwiftUICurrentConditions {
    
    let units: SwiftUIUnitsSettings
    let wind: Int
    let windDirection: String
    let windGust: Int
    let humidity: Int
    let pressure: Float
    let visibility: Float
    let ceiling: Int
    let sunrise: String
    let sunset: String
    let yesterdayHigh: Int
    let yesterdayLow: Int
    
    public init(units: SwiftUIUnitsSettings, wind: Int, windDirection: String,
                windGust: Int, humidity: Int, pressure: Float, visibility: Float,
                ceiling: Int, sunrise: String, sunset: String, yesterdayHigh: Int,
                yesterdayLow: Int
    ) {
        self.units = units
        self.wind = wind
        self.windDirection = windDirection
        self.windGust = windGust
        self.humidity = humidity
        self.pressure = pressure
        self.visibility = visibility
        self.ceiling = ceiling
        self.sunrise = sunrise
        self.sunset = sunset
        self.yesterdayHigh = yesterdayHigh
        self.yesterdayLow = yesterdayLow
    }
    
}
