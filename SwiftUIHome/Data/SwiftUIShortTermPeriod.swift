//
//  SwiftUIShortTermPeriod.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-04-06.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import Foundation

public class SwiftUIShortTermPeriod: Identifiable {
    
    public var id: UUID
    let periodNumber: Int
    let label: String
    let temperature: String
    let icon: String
    let feelsLike: String
    let pop: String
    let rainHeight: Int
    let snowHeight: Int
    let rainLabel: String
    let snowLabel: String
    
    public init(periodNumber: Int, label: String, temperature: String, icon: String, feelsLike: String, pop: String, rainHeight: Int, snowHeight: Int, rainLabel: String, snowLabel: String) {
        self.periodNumber = periodNumber
        self.id = UUID()
        self.label = label
        self.temperature = temperature
        self.icon = icon
        self.feelsLike = feelsLike
        self.pop = pop
        self.rainHeight = rainHeight
        self.snowHeight = snowHeight
        self.rainLabel = rainLabel
        self.snowLabel = snowLabel
    }
    
}
