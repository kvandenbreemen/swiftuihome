//
//  SwiftUICurrentObservations.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-14.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import Foundation

public struct SwiftUICurrentObservations {
    
    let temperature: Int
    let tempUnits: String
    let wxIconStr: String
    let feelsLike: Int
    let description: String
    
    public init(temperature: Int, tempUnits: String, wxIconStr: String, feelsLike: Int, description: String) {
        self.temperature = temperature
        self.tempUnits = tempUnits
        self.wxIconStr = wxIconStr
        self.feelsLike = feelsLike
        self.description = description
    }
    
}
