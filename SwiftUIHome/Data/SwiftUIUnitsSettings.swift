//
//  SwiftUIUnitsSettings.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-18.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import Foundation

public struct SwiftUIUnitsSettings {
    
    let wind: String
    let pressure: String
    let visibility: String
    let ceiling: String
    let rain: String
    let snow: String
    
    public init(wind: String, pressure: String, visibility: String, ceiling: String, rain: String, snow: String) {
        self.wind = wind
        self.pressure = pressure
        self.visibility = visibility
        self.ceiling = ceiling
        self.rain = rain
        self.snow = snow
    }
    
}
