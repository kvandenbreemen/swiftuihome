//
//  CircleBarButton.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-10.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import SwiftUI

struct CircleBarButton: View {
    
    @State var scaleFactor: CGFloat = CGFloat(1.0)
    @State var tapped = false
    
    private var onPress: ()->Void
    
    private var label: String
    
    private var imageAsset: String
    
    init(label: String = "Location", imageAsset: String = "swiftui_icon_home", onPress: @escaping (()->Void) = {}) {
        self.onPress = onPress
        self.label = label
        self.imageAsset = imageAsset
    }
    
    var body: some View {
        
        return VStack {
            Image(imageAsset).scaleEffect(2.0)
            Text(label)
                .font(.headline)
                .foregroundColor(Color.white)
                .multilineTextAlignment(.center)
                .lineLimit(1)
        }
        .frame(minWidth: 80, idealWidth: 100.0, maxWidth: 100, minHeight: 80, idealHeight: 100.0, maxHeight: 100, alignment: .center).padding([.leading, .trailing], 5)
        .background(Image("swiftui_button_bg_murky"))
        .clipShape(Circle())
        .contentShape(Circle()) .scaleEffect(scaleFactor).animation(Animation.default)
        .onTapGesture {
            if !self.tapped {
                self.scaleFactor = CGFloat(1.5)
            } else {
                self.scaleFactor = CGFloat(1.0)
            }
            
            self.tapped = !self.tapped
            
            self.onPress()
        }
        
    }
}

struct CircleBarButton_Previews: PreviewProvider {
    static var previews: some View {
        HStack {
            CircleBarButton()
        }
        .background(Image("swiftui_bar_bg_murky"))
        .previewLayout(.fixed(width: CGFloat(100), height: CGFloat(100)))
    }
}
