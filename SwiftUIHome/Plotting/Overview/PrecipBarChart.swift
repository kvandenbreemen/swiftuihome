//
//  PrecipBarChart.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-31.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import SwiftUI

fileprivate struct BarCapRect: Shape {
    
    func path(in rect: CGRect) -> Path {
        
        Path{
            path in
            path.move(to: CGPoint.init(x: rect.minX, y: rect.minY))
            path.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        }
        
        
    }
    
}

fileprivate struct BarRect: Shape {
    
    let height: CGFloat
    
    func path(in rect: CGRect) -> Path {
        Path{
            path in
            path.move(to: CGPoint.init(x: rect.minX, y: rect.minY))
            path.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
            path.addLine(to: CGPoint(x: rect.maxX, y: height))
            path.addLine(to: CGPoint(x: rect.minX, y: height))
            path.addLine(to: CGPoint(x: rect.minX, y: rect.minY))
        }
    }
}

struct BarSegment: View {
    
    let type: PrecipBarChartSegment.BarType
    let height: CGFloat
    
    var body: some View {
        
        var backgroundColor: Color
        var capColor: Color
        switch type {
        case .rain:
            backgroundColor = TWNSwiftUIDesignSystem.Colors.barChartRain
            capColor = TWNSwiftUIDesignSystem.Colors.barChartRainCap
        case .snow:
            backgroundColor = TWNSwiftUIDesignSystem.Colors.barChartSnow
            capColor = TWNSwiftUIDesignSystem.Colors.barChartSnowCap
        }
        
        return VStack(alignment: .center, spacing: 0) {
            
            GeometryReader { geometry in
                BarCapRect()
                    .stroke(capColor, style: StrokeStyle(lineWidth: 1.0))
                    .frame(width: geometry.size.width, height: 1)
                BarRect(height: self.height)
                    .fill(backgroundColor)
                    //.offset(x: 0, y: 0.5)
                
            }
            
        }
        
        
    }
    
    
}

struct PrecipBarChartSegment: View {
    
    struct Constants {
        static let maxBarHeight = 85
        static let maxHeight = maxBarHeight * 2
    }
    
    enum BarType {
        case rain
        case snow
    }
    
    let rain: Int
    let snow: Int
    
    var body: some View {
        
        GeometryReader { geometry in
            
            VStack(spacing: 0) {
                Spacer()
                BarSegment(type: .snow, height: CGFloat(self.snow)).frame(width: geometry.size.width, height: CGFloat(self.snow))
                BarSegment(type: .rain, height: CGFloat(self.rain)).frame(width: geometry.size.width, height: CGFloat(self.rain))
            }
        }
        
        
    }
}

struct ChartLabels: View {
    
    let rainLabel: String
    let snowLabel: String
    
    @ViewBuilder
    private var labels: some View {
        if rainLabel.isEmpty && snowLabel.isEmpty {
            return EmptyView()
        }
        
        return VStack {
            getLabel(snow: true, text: snowLabel)
            getLabel(snow: false, text: rainLabel)
        }
    }
    
    private func getLabel(snow: Bool, text: String) -> some View {
        if snow {
            if snowLabel.isEmpty {
                return AnyView(EmptyView())
            }
            return AnyView(HStack(spacing: 0) {
                Image("icon_snow").resizable().frame(width: 20, height: 20)
                Text(text).modifier(TWNSwiftUIDesignSystem.OverviewCellSmallText())
            })
        }
        if rainLabel.isEmpty {
            return AnyView(EmptyView())
        }
        return AnyView(HStack(spacing: 0) {
            Image("icon_bluerain_large").resizable().frame(width: 20, height: 20)
            Text(text).modifier(TWNSwiftUIDesignSystem.OverviewCellSmallText())
        })
    }
    
    var body: some View {
        VStack(spacing: 0) {
            getLabel(snow: true, text: snowLabel).scaleEffect(0.75)
            getLabel(snow: false, text: rainLabel).scaleEffect(0.75)
        }
    }
    
}

///  Bar chart segment that animates into view
struct AnimatedPrecipBarSegment: View {
    
    private struct Constants {
        static let maxLabelOffset = 50
        static let labelHeight = 20
    }
    
    let rain: Int
    let snow: Int
    let rainLabel: String
    let snowLabel: String
    
    @State var heightMultiplier: CGFloat = 0.0
    
    private func calculateLabelsOffset() -> CGFloat {
        var offset = Constants.maxLabelOffset
        
        if rain <= 0 {
            offset -= Constants.labelHeight
        }
        if snow <= 0 {
            offset -= Constants.labelHeight
        }
        
        return CGFloat(offset)
    }
    
    var body: some View {
        GeometryReader { geometry in
            VStack {
                Spacer()
                ChartLabels(
                    rainLabel: self.rain > 0 ? self.rainLabel : "",
                    snowLabel: self.snow > 0 ? self.snowLabel : ""
                )
                    .offset(x: 0, y: (geometry.size.height - self.calculateLabelsOffset()) - CGFloat(
                        (CGFloat(self.rain) * self.heightMultiplier) +
                        (CGFloat(self.snow) * self.heightMultiplier)
                    ))
                PrecipBarChartSegment(rain: Int(CGFloat(self.rain)*self.heightMultiplier),
                                      snow: Int(CGFloat(self.snow)*self.heightMultiplier))
            }
        }.onAppear {
            let animation = Animation.easeIn(duration: 1)
            return withAnimation(animation) {
                self.heightMultiplier = 1.0
            }
        }
    }
    
}

struct PrecipBarChart_Previews: PreviewProvider {
    static var previews: some View {
        
        let rain = 10
        let snow = 10
        
        return Group {
            
            BarCapRect().previewDisplayName("Bar Cap")
            BarRect(height: 14).previewDisplayName("Chart Bar")
            BarSegment(type: .rain, height: 22).previewDisplayName("Rain Bar")
            BarSegment(type: .snow, height: 44).previewDisplayName("Snow Bar")
            
            ChartLabels(rainLabel: "<1mm", snowLabel: "10cm")
                .background(Color.blue)
                .previewDisplayName("Chart Labels")
            
            PrecipBarChartSegment(rain: rain, snow: snow).previewDisplayName("Overview Precip Bar")
            
            AnimatedPrecipBarSegment(rain: rain, snow: snow,
                                     rainLabel: "< 1mm", snowLabel: "~1 cm"
            ).previewDisplayName("Animated Overview Precip Bar")
            
            AnimatedPrecipBarSegment(rain: 0, snow: 10, rainLabel: "test", snowLabel: "test").previewDisplayName("Snow Only Chart")
            
            AnimatedPrecipBarSegment(rain: 10, snow: 0, rainLabel: "test", snowLabel: "test").previewDisplayName("Rain Only Chart")
            
        }.previewLayout(.fixed(width: 100, height: 200))
            .background(Color.blue)
        
        
    }
}
