//
//  NetworkImage.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-05-01.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import SwiftUI

/// Fetches a network image and then returns it for display
class NetworkImageModel: ObservableObject {
    
    let imageInteractor: SwiftUIImageInteractorProtocol
    
    @Published
    var image: UIImage? = nil
    
    init(with imageInteractor: SwiftUIImageInteractorProtocol) {
        self.imageInteractor = imageInteractor
    }
    
    func update(with url: String) {
        imageInteractor.downloadImage(from: url) { [weak self] (image) in
            if let strongSelf = self {
                strongSelf.image = image
            }
        }
    }
    
}

struct NetworkImageView: View {
    
    @ObservedObject
    var model: NetworkImageModel
    
    var body: some View {
        Group {
            if model.image != nil {
                Image(uiImage: model.image!).resizable()
            } else {
                SwiftUILoadingView()
            }
        }
    }
}

struct NetworkImageView_Previews: PreviewProvider {
    static var previews: some View {
        
        let viewModel = NetworkImageModel.init(with: MockImageInteractor())
        
        let ret = Group {
            NetworkImageView(model: viewModel)
        }.background(Color.yellow)
        
        viewModel.update(with: "test")
        
        return ret
    }
}
