//
//  ImageInteractor.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-05-01.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import UIKit
import Foundation

public protocol SwiftUIImageInteractorProtocol {
    
    func downloadImage(from: String, completion: @escaping (UIImage)->Void)
    
}

class MockImageInteractor: SwiftUIImageInteractorProtocol {
    
    func downloadImage(from: String, completion: @escaping (UIImage) -> Void) {
        if let image = UIImage.init(named: "icon_bluerain_large") {
            completion(image)
        }
    }
    
}
