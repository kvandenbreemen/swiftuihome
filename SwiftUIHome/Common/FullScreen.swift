//
//  Code based on tutorial found at https://medium.com/swlh/customize-your-navigations-with-swiftui-173a72cd8a04
//

import SwiftUI
import Foundation

struct TransitionLink<Content, Destination>: View where Content: View, Destination: View {
    
    @Binding var isPresented: Bool
    var content: () -> Content
    var destination: () -> Destination
    
    init(isPresented: Binding<Bool>, @ViewBuilder destination: @escaping () -> Destination, @ViewBuilder content: @escaping () -> Content) {
        self.content = content
        self.destination = destination
        self._isPresented = isPresented
    }
    
    var body: some View {
        ZStack {
            if self.isPresented {
                self.destination()
                    .transition(.move(edge: .bottom))
            } else {
                self.content()
            }
        }
    }
    
}

public struct ModalLinkViewModifier<Destination>: ViewModifier where Destination: View {
    
    @Binding var isPresented: Bool
    var destination: () -> Destination
    
    public func body(content: Self.Content) -> some View {
        TransitionLink(isPresented: self.$isPresented,
                       destination: {
                        self.destination()
        },
        
        content: {
            content
        })
    }
    
}

extension View {
func modalLink<Destination: View>(isPresented: Binding<Bool>, destination: @escaping () -> Destination) -> some View {
        self.modifier(ModalLinkViewModifier(isPresented: isPresented, destination: destination))
    }
}
