//
//  SUCurrentObservationsInteractor.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-16.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import Combine
import Foundation

public protocol SUCurrentObservationsInteractorStrategy {
    
    func getCurrentObservations(for location: LocationPojo, _ onReceived: @escaping (SwiftUICurrentObservations)->Void)
    
}

class SUCurrentObservationsInteractor: SUCurrentObservationsInteractorStrategy {
    
    
    func getCurrentObservations(for location: LocationPojo, _ onReceived: (SwiftUICurrentObservations) -> Void) {
        
        let obs = SwiftUICurrentObservations.init(temperature: 23, tempUnits: "C", wxIconStr: "swiftui_sample_image_icon", feelsLike: 22, description: "Partly Cloudy and all Good")
        onReceived(obs)
        
    }
    
    
}
