//
//  SUITrendingNowInteractor.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-05-03.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import Foundation

public protocol SUITrendingNowInteractorStrategy {
    
    func getStoriesAndVideos(callback: @escaping SUInteractorCallback<[SwiftUITrendingItem]>)
    
}

class SUITrendingNowInteractor: SUITrendingNowInteractorStrategy {
    
    func getStoriesAndVideos(callback: @escaping SUInteractorCallback<[SwiftUITrendingItem]>) {
        
        let storiesAndSuff = [
            SwiftUITrendingItem.init(thumbnailURL: "https://live.staticflickr.com/8511/8568513185_4b90787b45.jpg", title: "Test News Story", type: .video),
            SwiftUITrendingItem.init(thumbnailURL: "https://live.staticflickr.com/8511/8568513185_4b90787b45.jpg", title: "Test News Story", type: .news)
        ]
        
        callback(storiesAndSuff, nil)
        
    }
    
    
    
    
}
