//
//  SUIShortTermForecastInteractor.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-04-06.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import Foundation

public protocol SUIShortTermForecastInteractorStrategy {
    
    func fetch(periods: Int, callback: @escaping SUInteractorCallback<[SwiftUIShortTermPeriod]>)
    
}

class SUIShortTermForecastInteractor: SUIShortTermForecastInteractorStrategy {
    
    static let rainHeights = [0,0,10,0,20,0,0]
    static let snowHeights = [0,0,3,0,11,0,15]
    
    func fetch(periods: Int, callback: @escaping SUInteractorCallback<[SwiftUIShortTermPeriod]>) {
        
        var periods = [SwiftUIShortTermPeriod]()
        
        periods.append(
            SwiftUIShortTermPeriod(periodNumber: 0, label: "Evening", temperature: "10", icon: "swiftui_sample_image_icon", feelsLike: "feels 18", pop: "20", rainHeight: 15, snowHeight: 10, rainLabel: "10 mm", snowLabel: "10 cm")
        )
        
        periods.append(
            SwiftUIShortTermPeriod(periodNumber: 1, label: "Overnight", temperature: "10", icon: "swiftui_sample_image_icon", feelsLike: "feels 18", pop: "20", rainHeight: 15, snowHeight: 10, rainLabel: "10 mm", snowLabel: "10 cm")
        )
        
        periods.append(
            SwiftUIShortTermPeriod(periodNumber: 2, label: "Morning", temperature: "10", icon: "swiftui_sample_image_icon", feelsLike: "feels 18", pop: "20", rainHeight: 15, snowHeight: 10, rainLabel: "10 mm", snowLabel: "10 cm")
        )
        
        periods.append(
            SwiftUIShortTermPeriod(periodNumber: 3, label: "Afternoon", temperature: "10", icon: "swiftui_sample_image_icon", feelsLike: "feels 18", pop: "20", rainHeight: 15, snowHeight: 10, rainLabel: "10 mm", snowLabel: "10 cm")
        )
        
        periods.append(
            SwiftUIShortTermPeriod(periodNumber: 4, label: "Evening", temperature: "10", icon: "swiftui_sample_image_icon", feelsLike: "feels 18", pop: "20", rainHeight: 15, snowHeight: 10, rainLabel: "10 mm", snowLabel: "10 cm")
        )
        
        periods.append(
            SwiftUIShortTermPeriod(periodNumber: 5, label: "Overnight", temperature: "10", icon: "swiftui_sample_image_icon", feelsLike: "feels 18", pop: "20", rainHeight: 15, snowHeight: 10, rainLabel: "10 mm", snowLabel: "10 cm")
        )
        
        callback(periods, nil)
        
    }
    
    
    
    
}
