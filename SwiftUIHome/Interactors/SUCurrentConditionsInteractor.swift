//
//  SUCurrentConditionsInteractor.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-19.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import Foundation

public protocol SUCurrentConditionsInteractorStrategy {
    
    func getCurrentConditions(location: LocationPojo, _ onReceived: @escaping (SwiftUICurrentConditions)->Void)
    
}

class SUCurrentConditionsInteractor: SUCurrentConditionsInteractorStrategy {
    func getCurrentConditions(location: LocationPojo, _ onReceived: @escaping (SwiftUICurrentConditions) -> Void) {
        
        onReceived(
                SwiftUICurrentConditions.init(
                    units: SwiftUIUnitsSettings.init(wind: "mph", pressure: "kPa", visibility: "km", ceiling: "m", rain: "in.", snow: "in."),
            wind: 8, windDirection: "NE", windGust: 12, humidity: 80, pressure: 103.0, visibility: 24.0, ceiling: 10000, sunrise: "07:25", sunset: "19:30", yesterdayHigh: 8, yesterdayLow: 1)
        )
        
    }
    
}
