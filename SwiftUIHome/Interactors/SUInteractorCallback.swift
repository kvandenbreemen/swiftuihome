//
//  InteractorCallback.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-27.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import Foundation

/// Default callback for sending data back to SwiftUI from an interactor
public typealias SUInteractorCallback<T> = (_ onSuccess: T?, _ error: SwiftUIErrors?) -> Void
