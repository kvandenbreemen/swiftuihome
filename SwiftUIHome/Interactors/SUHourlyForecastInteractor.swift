//
//  SUHourlyForecastInteractor.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-27.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import Foundation

public protocol SUHourlyForecastInteractorStrategy {
    
    func fetchHourlyForecast(for location: LocationPojo, with periods: Int, callback: @escaping SUInteractorCallback<[SwiftUIHourlyPeriod]>)
    
}

class SUHourlyForecastInteractor: SUHourlyForecastInteractorStrategy {
    
    static let rainHeights = [0,0,10,0,20,0,0]
    static let snowHeights = [0,0,3,0,11,0,15]
    
    func fetchHourlyForecast(for location: LocationPojo, with periods: Int, callback: @escaping SUInteractorCallback<[SwiftUIHourlyPeriod]>) {
        
        let unitSettings = SwiftUIUnitsSettings.init(wind: "mph", pressure: "kPa", visibility: "km", ceiling: "m", rain: "in", snow: "in")
        
        var ret = [SwiftUIHourlyPeriod]()
        for i in 0...6 {
            
            let rainHeight = SUHourlyForecastInteractor.rainHeights[i]
            let snowHeight = SUHourlyForecastInteractor.snowHeights[i]
            
            ret.append(SwiftUIHourlyPeriod(periodNumber: i, time: "\(i+1) am", icon: "swiftui_sample_small_icon", temperature: "\(i)", feelsLike: "feels \(i+2)", pop: "20",
                                           rainHeight: rainHeight,
                                           snowHeight: snowHeight,
                                           rainfallLabel: rainHeight == 0 ? "" : "\(rainHeight) mm",
                snowfallLabel: snowHeight == 0 ? "" : "\(snowHeight) cm", unitSettings: unitSettings, description: "Partly Cloudy", wind: 10, windGust: 25, windDirection: "se", rain: rainHeight, snow: snowHeight
                                           ))
        }
        
        callback(ret, nil)
        
    }
    
    
    
    
    
    
    
    
    
}
