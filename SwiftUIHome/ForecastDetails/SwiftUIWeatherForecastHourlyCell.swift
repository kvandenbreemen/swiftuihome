//
//  SwiftUIWeatherForecastHourlyCell.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-04-21.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import SwiftUI

struct SwiftUIWeatherForecastHourlyExpandedCell: View {
    
    let period: SwiftUIHourlyPeriod
    
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0.0) {
            Text("9am")
            HStack {
                Spacer()
                HStack(alignment: .lastTextBaseline, spacing: 0.0) {
                    Image(period.icon).scaleEffect(0.75).offset(x: 10, y: 10)
                    Text("\(period.temperature)°").modifier(TWNSwiftUIDesignSystem.WeatherDetailsTitle())
                    VStack(alignment: .leading) {
                        Text(period.description).modifier(TWNSwiftUIDesignSystem.WeatherDetailsDescription())
                        Text(period.feelsLike).modifier(TWNSwiftUIDesignSystem.WeatherDetailsExpandedMeasurementTitle())
                    }.padding([.leading], 10)
                }
                Spacer()
            }
            HStack {
                
                //  Wind / POP
                VStack(alignment: .leading, spacing: 0.0) {
                    
                    VStack(alignment: .leading) {
                        HStack(spacing: 0.0) {
                            Text("\(period.wind)").modifier(TWNSwiftUIDesignSystem.WeatherDetailsExpandedMeasurement())
                            VStack(alignment: .leading) {
                                Text(period.windDirection).modifier(TWNSwiftUIDesignSystem.OverviewCellMedText())
                                Text(period.unitSettings.wind).modifier(TWNSwiftUIDesignSystem.OverviewCellMedText())
                            }
                        }
                        Text("WIND").modifier(TWNSwiftUIDesignSystem.WeatherDetailsExpandedMeasurementTitle())
                    }
                    
                    VStack(alignment: .leading) {
                        HStack(alignment: .firstTextBaseline,spacing: 0.0) {
                            Text("\(period.pop)").modifier(TWNSwiftUIDesignSystem.WeatherDetailsExpandedMeasurement())
                            VStack(alignment: .leading) {
                                Image("icon_pop_cloud")
                                Text("%").modifier(TWNSwiftUIDesignSystem.OverviewCellMedText())
                            }
                        }
                        Text("P.O.P.").modifier(TWNSwiftUIDesignSystem.WeatherDetailsExpandedMeasurementTitle())
                    }
                }
                
                Spacer()
                
                //  Gust / Rain
                VStack(alignment: .leading, spacing: 0.0) {
                    
                    VStack(alignment: .leading) {
                        HStack(alignment: .bottom,spacing: 0.0) {
                            Text("\(period.windGust)").modifier(TWNSwiftUIDesignSystem.WeatherDetailsExpandedMeasurement())
                            Text(period.unitSettings.wind).modifier(TWNSwiftUIDesignSystem.OverviewCellMedText())
                                .offset(y: -5)
                        }
                        Text("WIND GUST").modifier(TWNSwiftUIDesignSystem.WeatherDetailsExpandedMeasurementTitle())
                    }
                    
                    VStack(alignment: .leading) {
                        HStack(alignment: .firstTextBaseline,spacing: 0.0) {
                            Text("\(period.pop)").modifier(TWNSwiftUIDesignSystem.WeatherDetailsExpandedMeasurement())
                            VStack(alignment: .leading) {
                                Image("icon_pop_cloud")
                                Text("\(period.unitSettings.rain)").modifier(TWNSwiftUIDesignSystem.OverviewCellMedText())
                            }
                        }
                        Text("RAIN").modifier(TWNSwiftUIDesignSystem.WeatherDetailsExpandedMeasurementTitle())
                    }
                }
            }
        }
    }
    
}

struct SwiftUIWeatherForecastHourlyCell: View {
    
    let period: SwiftUIHourlyPeriod
    
    @State
    private var showDetails: Bool = false
    
    @ViewBuilder
    var weekDay: some View {
        Text("TUESDAY").modifier(TWNSwiftUIDesignSystem.ObsTemperatureFeelsLikeStyle()).padding(3.0)
        Spacer()
    }
    
    @ViewBuilder
    func periodDateContent() -> some View {
        if period.periodNumber == 0 {
            weekDay
        } else {
            return EmptyView()
        }
    }
    
    func periodHour() -> some View {
        Text("9am")
    }
    
    var body: some View {
        VStack {
            if !showDetails {
                HStack{
                    periodDateContent()
                    Spacer()
                }
                HStack {
                    Text(period.time).modifier(TWNSwiftUIDesignSystem.OverviewCellMedText())
                    Spacer()
                    VStack(spacing: 0.0) {
                        HStack(alignment: .lastTextBaseline, spacing: 0.0) {
                            Image(period.icon).scaleEffect(0.75)
                            Text("\(period.temperature)°").modifier(TWNSwiftUIDesignSystem.WeatherDetailsTitle()).offset(x: -10, y: -10)
                        }
                        Text(period.feelsLike).modifier(TWNSwiftUIDesignSystem.OverviewCellMedText())
                    }
                    Spacer()
                    HStack {
                        Image("icon_pop_cloud")
                        Text(period.pop).modifier(TWNSwiftUIDesignSystem.OverviewCellMedText())
                    }
                }.padding([.leading, .trailing, .bottom], 10)
            } else {
                SwiftUIWeatherForecastHourlyExpandedCell(period: period)
            }
        }.onTapGesture {
            withAnimation {
                self.showDetails.toggle()
            }
        }
        
    }
}

struct SwiftUIWeatherForecastHourlyCell_Previews: PreviewProvider {
    static var previews: some View {
        
        let unitSettings = SwiftUIUnitsSettings.init(wind: "mph", pressure: "kPa", visibility: "km", ceiling: "m", rain: "in", snow: "in")
        let fakePeriod = SwiftUIHourlyPeriod(periodNumber: 0, time: "9am", icon: "swiftui_sample_small_icon", temperature: "12", feelsLike: "Feels like 11", pop: "20", rainHeight: 10, snowHeight: 1, rainfallLabel: "<1 mm", snowfallLabel: "~ 1cm", unitSettings: unitSettings, description: "Partly Cloudy", wind: 10, windGust: 20, windDirection: "se", rain: 15, snow: 1)
        
        return Group {
            SwiftUIWeatherForecastHourlyCell(period: fakePeriod)
            SwiftUIWeatherForecastHourlyExpandedCell(period: fakePeriod)
        }.background(Image("sui_bg_clearday").resizable())
            .previewLayout(.fixed(width: 350, height: 400))
            
    }
}
