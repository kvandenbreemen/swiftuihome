//
//  SwiftUIWeatherForecastDetail.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-04-08.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import SwiftUI

struct SwiftUIWeatherForecastDetail: View {
    
    let hourlyModel: SwiftUIOverviewHourlyViewModel
    let shortTermModel: SwiftUIOverview36HourViewModel

    init(di: SwiftUIInteractorProvider) {
        hourlyModel = SwiftUIOverviewHourlyViewModel(interactor: SUHourlyForecastInteractor(), location: di.location)
        shortTermModel = SwiftUIOverview36HourViewModel(interactor: SUIShortTermForecastInteractor())
    }
    
    var body: some View {
        Text("Weather Forecast Details")
    }
}

struct SwiftUIWeatherForecastDetail_Previews: PreviewProvider {
    static var previews: some View {
        SwiftUIWeatherForecastDetail(
            di: DefaultSwiftUIInteractorProvider()
        )
    }
}
