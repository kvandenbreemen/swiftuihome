//
//  DesignSystem.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-03-14.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import SwiftUI
import Foundation

struct TWNSwiftUIDesignSystem {
    
    private init(){}
    
    //  MARK:- Overview Styles
    
    struct ObsCurrentTemperatureStyle: ViewModifier {
        func body(content: Content) -> some View {
            content
                .font(.title)
                .foregroundColor(Color.white)
                .scaleEffect(2.5)
                .padding()
        }
    }
    
    struct ObsTemperatureUnitStyle: ViewModifier {
        func body(content: Content) -> some View {
            content
                .font(.headline)
                .foregroundColor(.white)
                .scaleEffect(1.5)
                .padding()
        }
    }
    
    struct ObsTemperatureFeelsLikeStyle: ViewModifier {
        
        func body(content: Content) -> some View {
            content
                .font(.callout)
                .foregroundColor(.white)
        }
        
    }
    
    struct ObsCurrentConditions: ViewModifier {
        
        func body(content: Content) -> some View {
            content
                .font(.headline)
                .foregroundColor(.white)
            
        }
        
    }
    
    struct CurrentConditionsUnitTitle: ViewModifier {
        func body(content: Content) -> some View {
            content.font(.footnote)
            .foregroundColor(.white)
        }
    }
    
    struct CurrentConditionsTitle: ViewModifier {
        func body(content: Content) -> some View {
            content.font(.body)
            .foregroundColor(.white)
        }
    }
    
    struct CurrentConditionsValue: ViewModifier {
        func body(content: Content) -> some View {
            content.font(.title)
                .scaleEffect(0.9)
            .foregroundColor(.white)
        }
    }
    
    struct TimeLabel: ViewModifier {
        func body(content: Content) -> some View {
            content.font(.none)
                .foregroundColor(.white)
        }
    }
    
    /// Title label above a cell on the overview
    struct OverviewCellTitle: ViewModifier {
        func body(content: Content) -> some View {
            content.font(Font.callout.bold())
                .foregroundColor(.white)
        }
    }
    
    struct OverviewCellSmallText: ViewModifier {
        func body(content: Content) -> some View {
            content.font(Font.footnote).foregroundColor(.white)
        }
    }
    
    struct OverviewCellMedText: ViewModifier {
        func body(content: Content) -> some View {
            content.font(Font.callout).foregroundColor(.white)
        }
    }
    
    struct OverviewCellLargeText: ViewModifier {
        func body(content: Content) -> some View {
            content.font(.title).foregroundColor(.white)
        }
    }
    
    //  MARK:-  Weather Detail Styles
    struct WeatherDetailsTitle: ViewModifier {
        func body(content: Content) -> some View {
            content.font(.system(size: 50)).foregroundColor(.white)
        }
    }
    
    struct WeatherDetailsDescription: ViewModifier {
        func body(content: Content) -> some View {
            content.font(.system(size: 20)).foregroundColor(.white)
        }
    }
    
    struct WeatherDetailsExpandedMeasurement: ViewModifier {
        func body(content: Content) -> some View {
            content.font(.system(size: 40)).foregroundColor(.white)
        }
    }
    
    struct WeatherDetailsExpandedUnits: ViewModifier {
        func body(content: Content) -> some View {
            content.font(Font.callout).foregroundColor(.white)
        }
    }
    
    struct WeatherDetailsExpandedMeasurementTitle: ViewModifier {
        func body(content: Content) -> some View {
            content.font(Font.caption).scaleEffect(1.3).foregroundColor(.white)
        }
    }
    
    struct Colors {
        
        static let darkBlue: Color = Color.init(.sRGB, red: 57/255.0, green: 87/255.0, blue: 131/255.0, opacity: 0.2)
        static let midBlue: Color = Color.init(.sRGB, red: 69/255.0, green: 112/255.0, blue: 170/255.0)
        static let transparent: Color = Color(.sRGB, red: 0, green: 0, blue: 0, opacity: 0.5)
        static let clear: Color = Color(.sRGB, red: 0, green: 0, blue: 0, opacity: 0.1)
        static let barChartRain: Color = Color(.sRGB, red: 51/255.0, green: 133/255.0, blue: 192/255.0)
        static let barChartSnow = Color(.sRGB, red: 1, green: 1, blue: 1, opacity: 0.55)
        static let barChartRainCap = Color(.sRGB, red: 89/255.0, green: 173/255.0, blue: 238/255.0)
        static let barChartSnowCap = Color(.sRGB, red: 1, green: 1, blue: 1, opacity: 1)
        
    }
    
}
