//
//  SwiftUIView.swift
//  SwiftUIHome
//
//  Created by Kevin VanDenBreemen on 2020-04-14.
//  Copyright © 2020 Pelmorex. All rights reserved.
//

import SwiftUI

struct SwiftUIView: View {
    var body: some View {
        VStack {
            HStack {
                Spacer()
            }
            .frame(minHeight: 10)
            .background(Color.green)
            
            Text("Hello Test")
                .fontWeight(.bold)
            
            Button(action: {
                print("You have pressed the Button")
            }){
                Text("Press the Button")
            }
            
            HStack{
                Spacer()
            }
            .frame(minHeight: 10)
            .background(Color.green)
        }
    }
}

struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        SwiftUIView()
    }
}
